package fr.alexpado;

import com.sun.net.httpserver.HttpServer;
import fr.alexpado.tools.graph.Graph;
import fr.alexpado.tools.graph.GraphSet;
import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.config.JSONConfiguration;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.events.ShutdownEvent;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.guildstat.GuildStatModule;
import ovh.akio.jdamodules.httprequest.CrossoutWebRequest;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.httprequest.HttpRequestModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.TranslationException;
import ovh.akio.jdamodules.translations.TranslationModule;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class RunBot extends JDABot {

    public static final long startTime = System.currentTimeMillis();

    private HttpServer server;

    private RunBot() throws Exception, TranslationException {
        super(AccountType.BOT);

        this.registerModule(new CrossoutModule(this));
        this.registerModule(new DatabaseModule(this));
        this.registerModule(new HttpRequestModule(this));
        this.registerModule(new ReporterModule(this));
        this.registerModule(new GuildStatModule(this));
        this.registerModule(new TranslationModule(this));

        this.login();

        this.server = HttpServer.create(new InetSocketAddress(8181), 0);
        server.createContext("/chart", exchange -> {
            CrossoutWebRequest request = new CrossoutWebRequest();
            List<String>       args    = Arrays.asList(exchange.getRequestURI().toString().replace("/chart/", "").split("/"));
            try {
                List<List<Integer>> priceSets = request.getPriceEvolution(Integer.parseInt(args.get(0)), Long.parseLong(args.get(1)));

                GraphSet sellSet = new GraphSet(priceSets.get(0), "SELL");
                GraphSet buySet  = new GraphSet(priceSets.get(1), "BUY");

                Graph         graph = new Graph(sellSet, buySet);
                BufferedImage img   = graph.draw(Color.GREEN, Color.RED);

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                ImageIO.write(img, "png", output);
                byte[] byteArray = output.toByteArray();

                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, byteArray.length);
                exchange.getResponseBody().write(byteArray);
                exchange.close();

            } catch (HttpException e) {
                e.printStackTrace();
            }
        });
        server.setExecutor(null);
        server.start();
    }

    @Override
    public void onShutdown(@Nonnull ShutdownEvent event) {
        this.server.stop(1);
    }

    public static void main(String[] args) throws Exception, TranslationException {
        new RunBot();
    }

}
