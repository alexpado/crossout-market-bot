package fr.alexpado.tools;

import fr.alexpado.tools.embed.EmbedPage;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.htmlparser.jericho.Renderer;
import net.htmlparser.jericho.Source;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.jdamodules.translations.Language;

import java.awt.*;
import java.util.List;

public class BotUtils {

    public static boolean checkPermission(Guild guild, TextChannel channel, Permission permission) {
        boolean permissionAllowed;
        permissionAllowed = guild.getSelfMember().hasPermission(permission);

        int rolePower = 0;

        for (Role role : guild.getSelfMember().getRoles()) {
            if (role.getPosition() > rolePower) {
                rolePower = role.getPosition();
                for (Permission rolePermission : role.getPermissions()) {
                    if (rolePermission == permission) {
                        permissionAllowed = true;
                    }
                }
                PermissionOverride permissionOverride = channel.getPermissionOverride(role);

                if (permissionOverride != null) {
                    for (Permission rolePermission : channel.getPermissionOverride(role).getAllowed()) {
                        if (rolePermission == permission) {
                            permissionAllowed = true;
                        }
                    }

                    for (Permission rolePermission : channel.getPermissionOverride(role).getDenied()) {
                        if (rolePermission == permission) {
                            permissionAllowed = false;
                        }
                    }
                }
            }
        }
        PermissionOverride permissionOverride = channel.getPermissionOverride(guild.getSelfMember());
        if (permissionOverride != null) {
            for (Permission permission1 : permissionOverride.getAllowed()) {
                if (permission1 == permission) {
                    permissionAllowed = true;
                }
            }

            for (Permission permission1 : permissionOverride.getDenied()) {
                if (permission1 == permission) {
                    permissionAllowed = false;
                }
            }
        }
        return permissionAllowed;
    }
    public static String removeHTML(String text) {
        Source   htmlSource = new Source(text);
        Renderer htmlRend   = new Renderer(htmlSource);
        return htmlRend.toString();
    }

    public static void loadWatchType(CommandEvent event, Watcher watcher, Integer whenIndex) {
        String watchType   = event.getArgs().get(whenIndex + 1);
        String priceType   = event.getArgs().get(whenIndex + 2);
        float  price       = Float.parseFloat(event.getArgs().get(whenIndex + 3).replace(",", "."));
        String watcherType = watchType.toUpperCase() + "_" + priceType.toUpperCase();
        watcher.setType(Watcher.WatchType.valueOf(watcherType));
        watcher.setPrice(price);
    }

    public static void sendItemListEmbed(List<Item> items, Message message, CommandEvent event, Language language) {
        new EmbedPage<Item>(message, items, 10) {
            @Override
            public EmbedBuilder getEmbed() {
                EmbedBuilder builder = new EmbedBuilder();
                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                builder.setColor(Color.WHITE);
                return builder;
            }
        };
    }


}
