package fr.alexpado.tools;


import ovh.akio.logger.Logger;

public class WindowBuilder {

    private static final int WIDTH = 40; // Inner Width

    private static final String HL = "═";

    private final String        title;
    private final StringBuilder window     = new StringBuilder();
    private       boolean       newSection = false;

    public WindowBuilder(String title) {
        this.title = title;
        this.buildHeader();
    }

    private void createSection() {
        String RS = "╠";
        this.window.append(RS);
        for (int i = 0 ; i < WIDTH ; i++) {
            this.window.append(HL);
        }
        String LS = "╣";
        this.window.append(LS).append("\n");
        this.newSection = false;
    }

    @SuppressWarnings("unused")
    public void startNewSection() {
        this.newSection = true;
    }

    public void addProperty(String key, String value) {
        if (this.newSection) {
            this.createSection();
        }
        int dotCount = WIDTH - (key.length() + 2 + value.length() + 2);
        if (dotCount < 0) {
            Logger.info(key);
            Logger.info(value);
            throw new IllegalArgumentException("Property and its value are overlapping.");
        }

        String VL = "║";
        this.window.append(VL).append(" ");
        this.window.append(key).append(" ");

        for (int i = 0 ; i < dotCount ; i++) {
            this.window.append(".");
        }

        this.window.append(" ").append(value);
        this.window.append(" ").append(VL);
        this.window.append("\n");
    }

    private void buildHeader() {
        String TL = "╔";
        this.window.append(TL).append(HL).append(" ").append(this.title).append(" ");
        int count = WIDTH - this.window.length();
        for (int i = 0 ; i < count ; i++) {
            this.window.append(HL);
        }
        String TR = "╗";
        this.window.append(HL).append(TR).append("\n");
    }

    @Override
    public String toString() {
        StringBuilder finalString = new StringBuilder();
        finalString.append(this.window.toString());
        String BL = "╚";
        finalString.append(BL);
        for (int i = 0 ; i < WIDTH ; i++) {
            finalString.append(HL);
        }
        String BR = "╝";
        finalString.append(BR);
        return finalString.toString();
    }

}
