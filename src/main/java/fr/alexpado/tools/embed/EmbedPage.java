package fr.alexpado.tools.embed;

import fr.alexpado.tools.BotUtils;
import fr.alexpado.tools.reaction.ReactionListener;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;

import java.util.ArrayList;
import java.util.List;

public abstract class EmbedPage<T> extends ReactionListener {

    private final ArrayList<String> items       = new ArrayList<>();
    private       int               currentPage;
    private       int               totalPage;
    private final int               count       = 10;

    protected EmbedPage(Message message, List<T> items, @SuppressWarnings("SameParameterValue") int timeout) {
        super(message, timeout);

        items.forEach(o -> this.items.add(o.toString()));
        this.currentPage = 1;

        float a = (float) items.size() / 10f;
        int   b = items.size() / 10;

        if (a > b) {
            totalPage = b + 1;
        } else {
            totalPage = b;
        }
        totalPage = totalPage == 0 ? 1 : totalPage;

        if (items.size() > 10) {
            if (BotUtils.checkPermission(message.getGuild(), message.getTextChannel(), Permission.MESSAGE_MANAGE)) {

                this.addAction("◀", reactionAction -> {
                    previousPage();
                    reactionAction.getReaction().removeReaction(reactionAction.getUser()).queue();
                });

                this.addAction("▶", reactionAction -> {
                    nextPage();
                    reactionAction.getReaction().removeReaction(reactionAction.getUser()).queue();
                });

                this.addAction("❌", reactionAction -> timeout(message));
                refreshEmbed();
                start();
            } else {
                refreshEmbed();
                message.getChannel().sendMessage("Impossible d'afficher les différentes pages. Pour cela je dois avoir la permission de gérer les messages.").queue();
            }
        } else {
            refreshEmbed();
        }
    }

    protected abstract EmbedBuilder getEmbed();

    private void refreshEmbed() {
        this.resetTimer();
        this.getMessage().editMessage(this.getEmbed().setDescription(getPageText()).setFooter("Page " + currentPage + "/" + totalPage, null).build()).queue();
    }

    private boolean isPageValid() {
        int[] pageBound = this.getPageBound(this.items.size(), this.currentPage, this.count);
        return pageBound != null;
    }

    private String getPageText() {

        int[] pageBound = this.getPageBound(this.items.size(), this.currentPage, this.count);
        if (pageBound == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = pageBound[0] ; i < pageBound[1] ; i++) {
            builder.append(this.items.get(i)).append("\n");
        }

        return builder.toString();
    }

    private void nextPage() {
        this.currentPage++;

        if (this.isPageValid()) {
            refreshEmbed();
        } else {
            this.currentPage--;
        }
    }

    private void previousPage() {
        this.currentPage--;

        if (this.isPageValid()) {
            refreshEmbed();
        } else {
            this.currentPage++;
        }
    }

    private int[] getPageBound(int size, int page, int count) {
        int startAt = (page - 1) * count;
        if (startAt > size || startAt < 0) {
            return null;
        }

        int endAt = startAt + count;
        if (endAt > size) {
            endAt = size;
        }
        return new int[]{startAt, endAt};
    }

}
