package fr.alexpado.tools.timers;

import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

public class TickTimer {

    private class Ticker extends TimerTask {

        private final Consumer<Void> onTick;

        Ticker(Consumer<Void> onTick) {
            this.onTick = onTick;
        }

        @Override
        public void run() {
            this.onTick.accept(null);
        }

    }

    public TickTimer(Consumer<Void> onTick, long period) {
        Ticker ticker = new Ticker(onTick);
        Timer  timer  = new Timer();
        timer.scheduleAtFixedRate(ticker, 0, period);
    }

}
