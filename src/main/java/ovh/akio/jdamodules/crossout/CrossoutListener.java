package ovh.akio.jdamodules.crossout;

import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class CrossoutListener extends ListenerAdapter {

    private CrossoutModule module;

    public CrossoutListener(CrossoutModule module) {
        this.module = module;
    }

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        this.module.onEnable(event.getJDA());
    }

}
