package ovh.akio.jdamodules.crossout;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.commands.JDACommandExecutor;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import ovh.akio.jdamodules.crossout.commands.ItemCommand;
import ovh.akio.jdamodules.crossout.commands.ItemSearchCommand;
import ovh.akio.jdamodules.crossout.commands.misc.HelpCommand;
import ovh.akio.jdamodules.crossout.commands.watchers.*;
import ovh.akio.jdamodules.crossout.entities.DiscordUser;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CrossoutModule extends JDAModule {

    public static final String inviteUrl = "https://discordapp.com/api/oauth2/authorize?client_id=500032551977746453&permissions=59456&scope=bot";

    private WatcherManager watcherManager;

    public CrossoutModule(JDABot bot) {
        super(bot);
    }

    /**
     * Called when the bot is logged in and all modules are loaded.
     */
    public void onEnable(JDA jda) {
        this.watcherManager = new WatcherManager(this);
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "crossout";
    }

    @Override
    public List<JDACommandExecutor> getCommandExecutors() {
        List<JDACommandExecutor> commands = new ArrayList<>();
        commands.add(new ItemCommand(this));
        commands.add(new ItemSearchCommand(this));
        commands.add(new WatchItemCommand(this));
        commands.add(new WatchListCommand(this));
        commands.add(new UnwatchCommand(this));
        commands.add(new WatcherSettingCommand(this));
        commands.add(new PauseWatchersCommand(this));
        commands.add(new HelpCommand(this));
        return commands;
    }

    @Nonnull
    @Override
    public List<ListenerAdapter> getListeners() {
        return Collections.singletonList(new CrossoutListener(this));
    }

    public WatcherManager getWatcherManager() {
        return watcherManager;
    }

    @Override
    public void onCommandExecuted(CommandEvent event) {
        try {
            DiscordUser user = new DiscordUser(event.getAuthor());

            Optional<DatabaseModule> optionalDatabaseModule = this.getBot().getModule(DatabaseModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                return;
            }

            DatabaseModule databaseModule = optionalDatabaseModule.get();
            Database       database       = databaseModule.createConnection();

            if (!user.select(database)) {
                user.insert(database);
            }
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
