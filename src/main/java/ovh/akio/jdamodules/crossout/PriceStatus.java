package ovh.akio.jdamodules.crossout;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;

public enum PriceStatus {

    GROW_GREEN("growgreen"),
    DROP_GREEN("dropgreen"),
    GROW_RED("growred"),
    DROP_RED("dropred"),
    UNCHANGED("unchanged");

    final String emoteCode;

    PriceStatus(String emoteCode) {
        this.emoteCode = emoteCode;
    }

    public String getAsMention(JDA jda) {
        Guild source = jda.getGuildById(508012982287073280L);
        assert source != null;
        return source.getEmotesByName(this.emoteCode, false).get(0).getAsMention();
    }

}
