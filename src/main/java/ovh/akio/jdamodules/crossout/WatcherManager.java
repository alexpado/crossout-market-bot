package ovh.akio.jdamodules.crossout;

import fr.alexpado.tools.timers.TickTimer;
import net.dv8tion.jda.api.entities.User;
import ovh.akio.jdamodules.crossout.entities.DiscordUser;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.httprequest.CrossoutWebRequest;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.httprequest.HttpRequestModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.TranslationException;
import ovh.akio.jdamodules.translations.TranslationModule;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class WatcherManager {

    private final TickTimer timer;

    private final DatabaseModule    databaseModule;
    private final ReporterModule    reporterModule;
    private final HttpRequestModule httpRequestModule;
    private final TranslationModule translationModule;

    WatcherManager(CrossoutModule module) {

        this.databaseModule = module.getBot().getModule(DatabaseModule.class).orElse(null);
        this.reporterModule = module.getBot().getModule(ReporterModule.class).orElse(null);
        this.httpRequestModule = module.getBot().getModule(HttpRequestModule.class).orElse(null);
        this.translationModule = module.getBot().getModule(TranslationModule.class).orElse(null);

        this.timer = new TickTimer(onTick -> this.runTick(), 60000);
    }

    public int getWatcherCount() throws SQLException {
        Database  database = this.databaseModule.createConnection();
        ResultSet rs       = database.query("SELECT COUNT(*) AS Total FROM Watchers");
        int       count    = 0;
        if (rs.next()) {
            count = rs.getInt("Total");
        }
        database.close(rs);
        database.close();
        return count;
    }

    private void runTick() {
        try {
            Database               database    = this.databaseModule.createConnection();
            List<Watcher>          watcherList = Watcher.getRefreshList(database);
            HashMap<Integer, Item> itemCache   = new HashMap<>();

            for (Watcher watcher : watcherList) {
                User target = this.reporterModule.getListener().getJda().getUserById(watcher.getUser());

                if (target == null) {
                    watcher.delete(database);
                    continue;
                }

                DiscordUser discordUser = new DiscordUser(target);
                if (!discordUser.select(database)) {
                    discordUser.insert(database);
                } else {
                    discordUser.update(database);
                }

                Language language = this.translationModule.findLanguage(target);

                Item item;
                if (itemCache.containsKey(watcher.getItem())) {
                    item = itemCache.get(watcher.getItem());
                } else {
                    CrossoutWebRequest web = this.httpRequestModule.createCrossoutRequest();
                    item = web.getItem(watcher.getItem());
                    itemCache.put(item.getId(), item);
                }

                if (item.isRemoved()) {
                    watcher.delete(database);
                    continue;
                }

                boolean shouldSendMessage = false;

                switch (watcher.getType()) {
                    case BUY_OVER:
                        shouldSendMessage = (item.getBuyPrice() / 100.f) > watcher.getPrice();
                        break;
                    case BUY_UNDER:
                        shouldSendMessage = (item.getBuyPrice() / 100.0f) < watcher.getPrice();
                        break;
                    case SELL_OVER:
                        shouldSendMessage = (item.getSellPrice() / 100.0f) > watcher.getPrice();
                        break;
                    case SELL_UNDER:
                        shouldSendMessage = (item.getSellPrice() / 100.0f) < watcher.getPrice();
                        break;
                    case NORMAL:
                        shouldSendMessage = true;
                        break;
                }

                watcher.updateLastExecutionTime();

                if (shouldSendMessage && !discordUser.isWatcherPaused()) {
                    target.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(item.getDiffEmbed(language, this.reporterModule.getListener().getJda(), watcher)).queue(message -> {
                        watcher.setLastBuyPrice(item.getBuyPrice());
                        watcher.setLastSellPrice(item.getSellPrice());
                        watcher.update(database);
                    }, throwable -> watcher.delete(database)));
                } else {
                    watcher.update(database);
                }
            }
            database.close();
        } catch (Exception | TranslationException | HttpException e) {
            this.reporterModule.reportError(e);
        }
    }

}
