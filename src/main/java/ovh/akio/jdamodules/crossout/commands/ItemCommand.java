package ovh.akio.jdamodules.crossout.commands;

import fr.alexpado.tools.BotUtils;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.OnlineStatus;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.httprequest.HttpRequestModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class ItemCommand extends TranslatableCommand {

    public ItemCommand(JDAModule module) {
        super(module, "item");
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {

        this.sendWaiting(event, message -> {

            if (event.getJDA().getPresence().getStatus() == OnlineStatus.DO_NOT_DISTURB) {
                this.sendError(message, language.getTranslation("general.crossout.crossoutDbOffline"));
                return;
            }

            HashMap<String, String> queryOptions = new HashMap<>();
            ArrayList<String>       messageArgs  = new ArrayList<>(event.getArgs());

            Optional<DatabaseModule>    optionalDatabaseModule    = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<HttpRequestModule> optionalHttpRequestModule = this.getModule().getBot().getModule(HttpRequestModule.class);
            Optional<ReporterModule>    optionalReporterModule    = this.getModule().getBot().getModule(ReporterModule.class);


            if (!optionalDatabaseModule.isPresent() || !optionalHttpRequestModule.isPresent()) {
                this.sendError(message, "Unable to load all required module.");
                return;
            }

            DatabaseModule    databaseModule    = optionalDatabaseModule.get();
            HttpRequestModule httpRequestModule = optionalHttpRequestModule.get();
            Database          database          = null;

            try {

                database = databaseModule.createConnection();

                if (messageArgs.contains("-r")) {
                    queryOptions.put("removedItems", "true");
                    messageArgs.remove("-r");
                }

                String itemName = String.join(" ", messageArgs.subList(1, messageArgs.size()));
                queryOptions.put("query", URLEncoder.encode(itemName, "UTF-8"));

                ArrayList<Item> items = httpRequestModule.createCrossoutRequest().find(queryOptions);

                if (items.size() == 0) {
                    this.sendError(message, language.getTranslation("general.crossout.noItemFound"));
                } else if (items.size() == 1) {
                    message.editMessage(items.get(0).getEmbed(language, event.getJDA())).queue();
                } else {
                    Optional<Item> optionalItem = items.stream().filter(crossoutItem -> crossoutItem.getName().equalsIgnoreCase(itemName)).findFirst();
                    if (optionalItem.isPresent()) {
                        message.editMessage(optionalItem.get().getEmbed(language, event.getJDA())).queue();
                    } else {
                        BotUtils.sendItemListEmbed(items, message, event, language);
                    }
                }
            } catch (SQLException exception) {
                this.sendError(message, this.getTranslation(language, "general.database.error"));
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(exception));
            } catch (HttpException e) {
                this.sendError(message, this.getTranslation(language, "general.http.error"));
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } catch (NumberFormatException e) {
                this.sendError(message, language.getTranslation("general.input.number"));
            } catch (UnsupportedEncodingException e) {
                this.sendError(message, language.getTranslation("general.error"), e.getMessage());
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, this.getTranslation(language, "general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
