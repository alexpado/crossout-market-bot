package ovh.akio.jdamodules.crossout.commands;

import fr.alexpado.tools.BotUtils;
import fr.alexpado.tools.embed.EmbedPage;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.crossout.gameobjects.*;
import ovh.akio.jdamodules.httprequest.CrossoutWebRequest;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.httprequest.HttpRequestModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class ItemSearchCommand extends TranslatableCommand {

    public ItemSearchCommand(JDAModule module) {
        super(module, "search");
    }


    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            Optional<HttpRequestModule> optionalHttpRequestModule = this.getModule().getBot().getModule(HttpRequestModule.class);
            Optional<ReporterModule>    optionalReporterModule    = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalHttpRequestModule.isPresent()) {
                this.sendError(message, "Unable to retrieve the HTTP Module.");
                return;
            }

            try {
                int                     rarityIndex       = event.getArgs().indexOf("--rarity");
                int                     categoryIndex     = event.getArgs().indexOf("--category");
                int                     factionIndex      = event.getArgs().indexOf("--faction");
                int                     typeIndex         = event.getArgs().indexOf("--type");
                int                     removedItemsIndex = event.getArgs().indexOf("--removedItem");
                int                     metaItemsIndex    = event.getArgs().indexOf("--metaItems");
                HashMap<String, String> params            = new HashMap<>();

                if (rarityIndex != -1) {
                    String rarityString = event.getArgs().get(rarityIndex + 1);
                    Rarity rarity       = Rarity.getClosest(rarityString);

                    if (rarity != null) {
                        params.put("rarity", rarity.getName().toLowerCase());
                    } else {
                        new EmbedPage<Rarity>(message, Rarity.asList(), 10) {
                            @Override
                            public EmbedBuilder getEmbed() {
                                EmbedBuilder builder = new EmbedBuilder();
                                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                                builder.setTitle(language.getTranslation("command.search.rarityList"));
                                builder.setColor(Color.WHITE);
                                return builder;
                            }
                        };
                        return;
                    }
                }

                if (categoryIndex != -1) {
                    String   categoryString = event.getArgs().get(categoryIndex + 1);
                    Category category       = Category.getClosest(categoryString);

                    if (category != null) {
                        params.put("category", category.getName().toLowerCase());
                    } else {
                        new EmbedPage<Category>(message, Category.asList(), 10) {
                            @Override
                            public EmbedBuilder getEmbed() {
                                EmbedBuilder builder = new EmbedBuilder();
                                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                                builder.setTitle(language.getTranslation("command.search.categoryList"));
                                builder.setColor(Color.WHITE);
                                return builder;
                            }
                        };
                        return;
                    }
                }

                if (factionIndex != -1) {
                    String  factionString = event.getArgs().get(factionIndex + 1);
                    Faction faction       = Faction.getClosest(factionString);

                    if (faction != null) {
                        params.put("faction", faction.getName().toLowerCase());
                    } else {
                        new EmbedPage<Faction>(message, Faction.asList(), 10) {
                            @Override
                            public EmbedBuilder getEmbed() {
                                EmbedBuilder builder = new EmbedBuilder();
                                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                                builder.setTitle(language.getTranslation("command.search.factionList"));
                                builder.setColor(Color.WHITE);
                                return builder;
                            }
                        };
                        return;
                    }
                }

                if (typeIndex != -1) {
                    String typeString = event.getArgs().get(typeIndex + 1);
                    Type   type       = Type.getClosest(typeString);

                    if (type != null) {
                        params.put("type", type.getName().toLowerCase());
                    } else {
                        new EmbedPage<Type>(message, Type.asList(), 10) {
                            @Override
                            public EmbedBuilder getEmbed() {
                                EmbedBuilder builder = new EmbedBuilder();
                                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                                builder.setTitle(language.getTranslation("command.search.typeList"));
                                builder.setColor(Color.WHITE);
                                return builder;
                            }
                        };
                        return;
                    }
                }

                if (removedItemsIndex != -1) {
                    params.put("removedItems", "true");
                }

                if (metaItemsIndex != -1) {
                    params.put("metaItems", "true");
                }

                CrossoutWebRequest request = optionalHttpRequestModule.get().createCrossoutRequest();
                List<Item>         items   = request.find(params);

                if (items.size() == 0) {
                    this.sendError(message, language.getTranslation("general.crossout.noItemFound"));
                } else {
                    BotUtils.sendItemListEmbed(items, message, event, language);
                }
            } catch (HttpException e) {
                this.sendError(message, this.getTranslation(language, "general.http.error"));
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } catch (IndexOutOfBoundsException e) {
                this.sendError(message, this.getTranslation(language, "general.input.syntax"));
            }
        });
    }

}
