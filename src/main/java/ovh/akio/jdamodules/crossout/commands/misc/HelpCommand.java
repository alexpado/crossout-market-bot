package ovh.akio.jdamodules.crossout.commands.misc;

import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.awt.*;

public class HelpCommand extends TranslatableCommand {

    public HelpCommand(JDAModule module) {
        super(module, "help");
    }


    /**
     * Called when this command is executed in the chat.
     *
     * @param event
     *         Event that triggered this command
     * @param language
     *         Language to use while executing this command
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            EmbedBuilder builder = new EmbedBuilder();

            builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
            builder.setDescription(language.getTranslation("command.help.embed.description"));
            builder.setColor(Color.WHITE);

            this.getModule().getBot().getCommandManager().getCommands().forEach(command -> {
                if (command instanceof TranslatableCommand) {
                    builder.addField(command.getLabel(), ((TranslatableCommand) command).getDescription(language), false);
                } else {
                    builder.addField(command.getLabel(), command.getDescription(), false);
                }
            });
            message.editMessage(builder.build()).queue();
        });
    }

}
