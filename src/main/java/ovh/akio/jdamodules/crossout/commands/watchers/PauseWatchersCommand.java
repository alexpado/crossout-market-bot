package ovh.akio.jdamodules.crossout.commands.watchers;

import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.crossout.entities.DiscordUser;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class PauseWatchersCommand extends TranslatableCommand {

    public PauseWatchersCommand(JDAModule module) {
        super(module, "pausewatchers");
    }

    /**
     * All aliases that can trigger this command in the chat. * NOTE : Labels have a higher priority on the aliases. If
     * a command is using one of your aliases defined here * as label, the label will be used to execute the command and
     * your alias will be ignored.
     */
    @Override
    public List<String> getAliases() {
        List<String> aliases = super.getAliases();
        aliases.add("pausewatcher");
        return aliases;
    }

    /**
     * Called when this command is executed in the chat.
     *
     * @param event
     *         Event that triggered this command
     * @param language
     *         Language associated with this event
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, language.getTranslation("general.database.error"));
                return;
            }

            DatabaseModule dbModule = optionalDatabaseModule.get();
            Database       database = null;
            try {
                database = dbModule.createConnection();
                DiscordUser discordUser  = new DiscordUser(event.getAuthor());
                boolean     shouldUpdate = discordUser.select(database);

                discordUser.setName(event.getAuthor().getName());
                discordUser.setWatcherPaused(!discordUser.isWatcherPaused());

                if (shouldUpdate) {
                    discordUser.update(database);
                } else {
                    discordUser.insert(database);
                }

                if (discordUser.isWatcherPaused()) {
                    this.sendInfo(message, this.getTranslation(language, "command.pausewatcher.paused"));
                } else {
                    this.sendInfo(message, this.getTranslation(language, "command.pausewatcher.resumed"));
                }

            } catch (SQLException e) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                this.sendError(message, this.getTranslation(language, "general.database.error"));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                        this.sendError(message, this.getTranslation(language, "general.database.wtf"));
                    }
                }
            }
        });
    }

}
