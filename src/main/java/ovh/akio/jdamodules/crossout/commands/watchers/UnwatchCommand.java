package ovh.akio.jdamodules.crossout.commands.watchers;


import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.sql.SQLException;
import java.util.Optional;

public class UnwatchCommand extends TranslatableCommand {

    public UnwatchCommand(JDAModule module) {
        super(module, "unwatch");
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, language.getTranslation("general.database.error"));
                return;
            }

            DatabaseModule dbModule = optionalDatabaseModule.get();
            Database       database = null;

            try {
                database = dbModule.createConnection();

                if (event.getArgs().size() > 1) {
                    int     watcherId = Integer.parseInt(event.getArgs().get(1));
                    Watcher watcher   = new Watcher(event.getAuthor(), watcherId);
                    if (watcher.select(database)) {
                        if (watcher.delete(database)) {
                            this.sendInfo(message, this.getTranslation(language, "command.unwatch.removed"), watcher.getItemName());
                        } else {
                            this.sendError(message, language.getTranslation("general.database.error"));
                        }
                    } else {
                        this.sendError(message, this.getTranslation(language, "general.watcher.notFound"));
                    }
                } else {
                    this.sendError(message, this.getTranslation(language, "general.input.syntax"));
                }
            } catch (SQLException exception) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(exception));
                this.sendError(message, this.getTranslation(language, "general.database.error"));
            } catch (NumberFormatException e) {
                this.sendError(message, this.getTranslation(language, "general.input.number"));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, this.getTranslation(language, "general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
