package ovh.akio.jdamodules.crossout.commands.watchers;

import fr.alexpado.tools.BotUtils;
import fr.alexpado.tools.timers.TimeConverter;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.OnlineStatus;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.httprequest.HttpRequestModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class WatchItemCommand extends TranslatableCommand {

    public WatchItemCommand(JDAModule module) {
        super(module, "watch");
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {

            if (event.getJDA().getPresence().getStatus() == OnlineStatus.DO_NOT_DISTURB) {
                this.sendError(message, language.getTranslation("general.crossout.crossoutDbOffline"));
                return;
            }

            HashMap<String, String> queryOptions = new HashMap<>();

            Optional<DatabaseModule>    optionalDatabaseModule    = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<HttpRequestModule> optionalHttpRequestModule = this.getModule().getBot().getModule(HttpRequestModule.class);
            Optional<ReporterModule>    optionalReporterModule    = this.getModule().getBot().getModule(ReporterModule.class);


            if (!optionalDatabaseModule.isPresent() || !optionalHttpRequestModule.isPresent()) {
                this.sendError(message, "Unable to load all required module.");
                return;
            }

            DatabaseModule    databaseModule    = optionalDatabaseModule.get();
            HttpRequestModule httpRequestModule = optionalHttpRequestModule.get();
            Database          database          = null;

            try {
                database = databaseModule.createConnection();

                String itemName;
                int    whenIndex = event.getArgs().indexOf("when");

                if (whenIndex == -1) {
                    itemName = String.join(" ", event.getArgs().subList(1, event.getArgs().size()));
                } else {
                    itemName = String.join(" ", event.getArgs().subList(1, whenIndex));
                }
                queryOptions.put("query", URLEncoder.encode(itemName, "UTF-8"));
                ArrayList<Item> items = httpRequestModule.createCrossoutRequest().find(queryOptions);

                if (items.size() == 0) {
                    this.sendError(message, language.getTranslation("general.crossout.noItemFound"));
                } else if (items.size() == 1) {

                    Watcher watcher = new Watcher(event.getAuthor(), items.get(0));

                    watcher.setLastSellPrice(items.get(0).getSellPrice());
                    watcher.setLastBuyPrice(items.get(0).getBuyPrice());

                    if (whenIndex != -1) {
                        BotUtils.loadWatchType(event, watcher, whenIndex);
                    }

                    int everyIndex = event.getArgs().indexOf("every");

                    if (everyIndex != -1) {
                        String every = event.getArgs().get(everyIndex + 1);
                        watcher.setRepeatEvery(TimeConverter.fromString(every));
                    }

                    if (watcher.insert(database)) {
                        this.sendInfo(message, language.getTranslation("command.watch.added"), items.get(0).getName());
                    } else {
                        this.sendError(message, language.getTranslation("general.database.error"));
                    }
                } else {
                    this.sendError(message, language.getTranslation("command.watch.multipleFound"));
                }


            } catch (SQLException exception) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(exception));
                this.sendError(message, language.getTranslation("general.database.error"));
            } catch (NumberFormatException e) {
                this.sendError(message, language.getTranslation("general.input.price"));
            } catch (UnsupportedEncodingException e) {
                this.sendError(message, language.getTranslation("general.error"), e.getMessage());
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } catch (HttpException e) {
                this.sendError(message, language.getTranslation("general.http.error"));
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } catch (IllegalArgumentException e) {
                this.sendError(message, language.getTranslation("general.watchers.settings"));
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, language.getTranslation("general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
