package ovh.akio.jdamodules.crossout.commands.watchers;

import fr.alexpado.tools.embed.EmbedPage;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class WatchListCommand extends TranslatableCommand {

    public WatchListCommand(JDAModule module) {
        super(module, "watchlist");
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {

            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);


            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, "Unable to load database module.");
                return;
            }

            DatabaseModule databaseModule = optionalDatabaseModule.get();
            Database       database       = null;

            try {
                database = databaseModule.createConnection();
                ArrayList<Watcher> watcherList = new ArrayList<>(Watcher.getUserWatchers(database, event.getAuthor()));

                watcherList.forEach(watcher -> watcher.setLanguage(language));

                if (watcherList.size() > 0) {
                    new EmbedPage<Watcher>(message, watcherList, 10) {
                        @Override
                        public EmbedBuilder getEmbed() {
                            EmbedBuilder builder = new EmbedBuilder();
                            builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, event.getJDA().getSelfUser().getAvatarUrl());
                            builder.setColor(Color.WHITE);
                            return builder;
                        }
                    };
                } else {
                    this.sendWarn(message, language.getTranslation("command.watchlist.empty"));
                }

            } catch (SQLException exception) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(exception));
                this.sendError(message, this.getTranslation(language, "general.database.error"));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, this.getTranslation(language, "general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
