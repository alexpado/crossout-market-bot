package ovh.akio.jdamodules.crossout.commands.watchers;


import fr.alexpado.tools.BotUtils;
import fr.alexpado.tools.timers.TimeConverter;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class WatcherSettingCommand extends TranslatableCommand {

    public WatcherSettingCommand(JDAModule module) {
        super(module, "watchersettings");
    }

    /**
     * All aliases that can trigger this command in the chat. * NOTE : Labels have a higher priority on the aliases. If
     * a command is using one of your aliases defined here * as label, the label will be used to execute the command and
     * your alias will be ignored.
     */
    @Override
    public List<String> getAliases() {
        List<String> aliases = super.getAliases();
        aliases.add("watchersetting");
        return aliases;
    }

    /**
     * Called when this command is executed in the chat.
     * <p>
     * TODO : Make args detection (when, every) with regex instead of indexes.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {

            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, language.getTranslation("general.database.error"));
                return;
            }

            DatabaseModule dbModule = optionalDatabaseModule.get();
            Database       database = null;

            try {
                database = dbModule.createConnection();

                if (event.getArgs().size() > 1) {
                    int watcherId   = Integer.parseInt(event.getArgs().get(1));
                    int whenIndex   = event.getArgs().indexOf("when");
                    int alwaysIndex = event.getArgs().indexOf("always");
                    int everyIndex  = event.getArgs().indexOf("every");

                    Watcher watcher = new Watcher(event.getAuthor(), watcherId);
                    if (watcher.select(database)) {

                        if (whenIndex != -1) {
                            BotUtils.loadWatchType(event, watcher, whenIndex);
                        } else if (alwaysIndex != -1) {
                            watcher.setType(Watcher.WatchType.NORMAL);
                            watcher.setPrice(0);
                        }

                        if (everyIndex != -1) {
                            String every = event.getArgs().get(everyIndex + 1);
                            watcher.setRepeatEvery(TimeConverter.fromString(every));
                        }

                        if (watcher.update(database)) {
                            this.sendInfo(message, this.getTranslation(language, "general.watchers.updated"));
                        } else {
                            this.sendError(message, language.getTranslation("general.database.error"));
                        }
                    } else {
                        this.sendError(message, this.getTranslation(language, "general.watchers.notFound"));
                    }
                } else {
                    this.sendError(message, this.getTranslation(language, "general.input.syntax"));
                }

            } catch (SQLException exception) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(exception));
                this.sendError(message, this.getTranslation(language, "general.database.error"));
            } catch (NumberFormatException e) {
                this.sendError(message, language.getTranslation("general.input.number"));
            } catch (IllegalArgumentException e) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                this.sendError(message, language.getTranslation("general.watchers.settings"));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, language.getTranslation("general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
