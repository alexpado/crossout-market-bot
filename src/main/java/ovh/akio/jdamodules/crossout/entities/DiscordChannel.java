package ovh.akio.jdamodules.crossout.entities;

import net.dv8tion.jda.api.entities.TextChannel;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseEntity;
import ovh.akio.jdamodules.translations.BotLocale;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DiscordChannel implements DatabaseEntity {

    private final long      id;
    private final long      guild;
    private       String    name;
    private       BotLocale locale   = BotLocale.ENGLISH;
    private       boolean   announce = false;

    public DiscordChannel(TextChannel channel) {
        this.id    = channel.getIdLong();
        this.guild = channel.getGuild().getIdLong();
        this.name  = channel.getName();
    }

    @Override
    public boolean select(Database database) throws SQLException {
        ResultSet rs = database.query("SELECT * FROM Channels WHERE id = ? AND guild = ?", this.id, this.guild);
        if (rs.next()) {
            this.name     = rs.getString("name");
            this.locale   = BotLocale.get(rs.getString("language"));
            this.announce = rs.getBoolean("announce");
            database.close(rs);
            return true;
        } else {
            database.close(rs);
            return false;
        }
    }

    @Override
    public boolean insert(Database database) {
        return database.execute("INSERT INTO Channels (id, guild, name, language, announce) VALUE (?, ?, ?, ?, ?)", this.id, this.guild, this.name, this.locale.getTag(), this.announce);
    }

    @Override
    public boolean delete(Database database) {
        return database.execute("DELETE FROM Channels WHERE id = ? AND guild = ?", this.id, this.guild);
    }

    @Override
    public boolean update(Database database) {
        return database.execute("UPDATE Channels SET name = ?, language = ?, announce = ? WHERE id = ? AND guild = ?", this.name, this.locale.getTag(), this.announce, this.id, this.guild);
    }

    public BotLocale getLocale() {
        return locale;
    }

    public void setLocale(BotLocale locale) {
        this.locale = locale;
    }

}
