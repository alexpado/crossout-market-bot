package ovh.akio.jdamodules.crossout.entities;

import net.dv8tion.jda.api.entities.Guild;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseEntity;
import ovh.akio.jdamodules.translations.BotLocale;

import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("unused")
public class DiscordGuild implements DatabaseEntity {

    private final long      id;
    private final String    name;
    private       BotLocale locale = BotLocale.ENGLISH;
    private final long      since;

    public DiscordGuild(Guild guild) {
        this.id    = guild.getIdLong();
        this.name  = guild.getName();
        this.since = guild.getSelfMember().getTimeJoined().toEpochSecond();
    }

    public BotLocale getLocale() {
        return locale;
    }

    public void setLocale(BotLocale locale) {
        this.locale = locale;
    }

    @Override
    public boolean select(Database database) throws SQLException {
        ResultSet rs = database.query("SELECT language FROM Guilds WHERE id = ?", this.id);
        if (rs.next()) {
            this.locale = BotLocale.get(rs.getString("language"));
            database.close(rs);
            return true;
        } else {
            database.close(rs);
            return false;
        }
    }

    @Override
    public boolean insert(Database database) {
        return database.execute("INSERT INTO Guilds (id, name, since) VALUE (?, ?, ?)", this.id, this.name, this.since);
    }

    @Override
    public boolean delete(Database database) {
        return database.execute("DELETE FROM Guilds WHERE id = ?", this.id);
    }

    public boolean update(Database database) {
        return database.execute("UPDATE Guilds SET name = ?, language = ? WHERE id = ?", this.name, this.locale.getTag(), this.id);
    }

}
