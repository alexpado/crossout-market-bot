package ovh.akio.jdamodules.crossout.entities;

import net.dv8tion.jda.api.entities.User;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseEntity;
import ovh.akio.jdamodules.translations.BotLocale;

import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class DiscordUser implements DatabaseEntity {

    private final long      id;
    private       String    name;
    private       BotLocale locale        = BotLocale.ENGLISH;
    private       boolean   watcherPaused = false;

    public DiscordUser(User user) {
        this.id   = user.getIdLong();
        this.name = user.getName();
    }


    public void setName(String name) {
        this.name = name;
    }

    public BotLocale getLocale() {
        return locale;
    }

    public boolean isWatcherPaused() {
        return watcherPaused;
    }

    public void setWatcherPaused(boolean watcherPaused) {
        this.watcherPaused = watcherPaused;
    }

    @Override
    public boolean select(Database database) throws SQLException {
        ResultSet rs = database.query("SELECT language, watchersPaused FROM Users WHERE id = ?", this.id);
        if (rs.next()) {
            this.locale        = BotLocale.get(rs.getString("language"));
            this.watcherPaused = rs.getBoolean("watchersPaused");
            database.close(rs);
            return true;
        } else {
            database.close(rs);
            return false;
        }
    }

    @Override
    public boolean insert(Database database) {
        return database.execute("INSERT INTO Users (id, name, language, watchersPaused) VALUE (?, ?, ?, ?)", this.id, this.name, this.locale.getTag(), this.watcherPaused);
    }

    @Override
    public boolean delete(Database database) {
        return database.execute("DELETE FROM Users WHERE id = ?", this.id);
    }

    public boolean update(Database database) {
        return database.execute("UPDATE Users SET name = ?, language = ?, watchersPaused = ? WHERE id = ?", this.name, this.locale.getTag(), this.watcherPaused, this.id);
    }

}
