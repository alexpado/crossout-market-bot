package ovh.akio.jdamodules.crossout.entities;

import fr.alexpado.tools.timers.TimeConverter;
import net.dv8tion.jda.api.entities.User;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseEntity;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.logger.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Watcher implements DatabaseEntity {

    private int       id;
    private long      user;
    private int       item;
    private String    itemName;
    private int       lastSellPrice = 0;
    private int       lastBuyPrice  = 0;
    private WatchType type          = WatchType.NORMAL;
    private float     price         = 0;
    private long      repeatEvery   = 300000;
    private long      lastExecution = System.currentTimeMillis();

    public Watcher(User user, Item item) {
        this.user     = user.getIdLong();
        this.item     = item.getId();
        this.itemName = item.getName();
    }


    public Watcher(User user, int id) {
        this.id   = id;
        this.user = user.getIdLong();
    }

    private Watcher(ResultSet source) throws SQLException {
        this.id   = source.getInt("id");
        this.user = source.getLong("user");
        this.loadFromRS(source);
    }

    private void loadFromRS(ResultSet rs) throws SQLException {
        this.item          = rs.getInt("item");
        this.itemName      = rs.getString("itemName");
        this.lastSellPrice = rs.getInt("lastSellPrice");
        this.lastBuyPrice  = rs.getInt("lastBuyPrice");
        this.type          = WatchType.valueOf(rs.getString("type"));
        this.price         = rs.getInt("price");
        this.repeatEvery   = rs.getInt("repeatEvery");
        this.lastExecution = rs.getLong("lastExecution");
    }

    public static List<Watcher> getRefreshList(Database database) throws SQLException {
        ResultSet     rs       = database.query("SELECT * FROM Watchers WHERE lastExecution + repeatEvery < ?", System.currentTimeMillis());
        List<Watcher> watchers = new ArrayList<>();
        while (rs.next()) {
            watchers.add(new Watcher(rs));
        }
        database.close(rs);
        return watchers;
    }

    public static List<Watcher> getUserWatchers(Database database, User user) throws SQLException {
        ResultSet     rs       = database.query("SELECT * FROM Watchers WHERE user = ?", user.getIdLong());
        List<Watcher> watchers = new ArrayList<>();
        while (rs.next()) {
            watchers.add(new Watcher(rs));
        }
        database.close(rs);
        return watchers;
    }

    public int getId() {
        return id;
    }

    public long getUser() {
        return user;
    }

    public int getItem() {
        return item;
    }

    public String getItemName() {
        return itemName;
    }

    public int getLastSellPrice() {
        return lastSellPrice;
    }

    public void setLastSellPrice(int lastSellPrice) {
        this.lastSellPrice = lastSellPrice;
    }

    public int getLastBuyPrice() {
        return lastBuyPrice;
    }

    public void setLastBuyPrice(int lastBuyPrice) {
        this.lastBuyPrice = lastBuyPrice;
    }

    public WatchType getType() {
        return type;
    }

    public void setType(WatchType type) {
        this.type = type;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    private long getRepeatEvery() {
        return repeatEvery;
    }

    public void setRepeatEvery(long repeatEvery) {
        this.repeatEvery = repeatEvery < 300000 ? 300000 : repeatEvery;
    }

    private long getLastExecution() {
        return lastExecution;
    }

    public void updateLastExecutionTime() {
        this.lastExecution = System.currentTimeMillis();
    }

    public boolean update(Database database) {
        return database.execute("UPDATE Watchers SET lastSellPrice = ?, lastBuyPrice = ?, type = ?, price = ?, repeatEvery =" + " ?, lastExecution = ? WHERE id = ? AND user = ?", this.getLastSellPrice(), this.getLastBuyPrice(), this.getType().name(), this.getPrice(), this.getRepeatEvery(), this.getLastExecution(), this.getId(), this.getUser());
    }

    @Override
    public boolean select(Database database) throws SQLException {
        ResultSet rs = database.query("SELECT * FROM Watchers WHERE id = ? AND user = ?", this.getId(), this.getUser());
        if (rs.next()) {
            this.loadFromRS(rs);
            database.close(rs);
            return true;
        } else {
            database.close(rs);
            return false;
        }
    }

    @Override
    public boolean insert(Database database) {
        return database.execute("INSERT INTO Watchers (user, item, itemName, lastSellPrice, lastBuyPrice, type, price, " + "repeatEvery, lastExecution) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?)", this.getUser(), this.getItem(), this.getItemName(), this.getLastSellPrice(), this.getLastBuyPrice(), this.getType().name(), this.getPrice(), this.getRepeatEvery(), this.getLastExecution());
    }

    @Override
    public boolean delete(Database database) {
        Logger.info("Deleting watcher " + this.getId() + " from user " + this.getUser());
        return database.execute("DELETE FROM Watchers WHERE id = ?", this.getId());
    }

    private Language language;

    public void setLanguage(Language language) {
        this.language = language;
    }

    @SuppressWarnings("unused")
    public Language getLanguage() {
        return language;
    }

    @Override
    public String toString() {
        switch (this.getType()) {
            case SELL_UNDER:
            case SELL_OVER:
            case BUY_OVER:
            case BUY_UNDER:
                return "[" + this.getId() + "] " + this.getItemName() + " " + String.format(this.language.getTranslation(this.getType().getRepresentation()), this.getPrice(), new TimeConverter(this.getRepeatEvery() / 1000).toString());
            default:
                return "[" + this.getId() + "] " + this.getItemName() + " " + String.format(this.language.getTranslation(this.getType().getRepresentation()), new TimeConverter(this.getRepeatEvery() / 1000).toString());
        }
    }

    public enum WatchType {
        BUY_OVER("general.watchers.buy.over"),
        BUY_UNDER("general.watchers.buy.under"),
        SELL_OVER("general.watchers.sell.over"),
        SELL_UNDER("general.watchers.sell.under"),
        NORMAL("general.watchers.normal");

        final String representation;

        WatchType(String representation) {
            this.representation = representation;
        }

        String getRepresentation() {
            return representation;
        }
    }

}
