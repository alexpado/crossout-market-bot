package ovh.akio.jdamodules.crossout.gameobjects;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public enum Category {

    CABINS(1, "Cabins"),
    WEAPONS(2, "Weapons"),
    HARDWARE(3, "Hardware"),
    MOVEMENT(4, "Movement"),
    STRUCTURE(5, "Structure"),
    DECOR(6, "Decor"),
    DYES(7, "Dyes"),
    RESOURCES(8, "Resources"),
    CUSTOMIZATION(9, "Customization");

    private final int    id;
    private final String name;

    Category(int id, String name) {
        this.id   = id;
        this.name = name;
    }

    public static Category get(int id) {
        for (Category value : Category.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }

    public static Category getClosest(String str) {
        for (Category value : Category.values()) {
            if (value.getName().toLowerCase().startsWith(str.toLowerCase())) {
                return value;
            }
        }
        return null;
    }

    public static List<Category> asList() {
        return Arrays.asList(Category.values());
    }

    /**
     * Get the id of the current Category.
     */
    private int getId() {
        return id;
    }

    /**
     * Get the name of the current Category.
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
