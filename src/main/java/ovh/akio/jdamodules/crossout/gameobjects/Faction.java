package ovh.akio.jdamodules.crossout.gameobjects;

import java.util.Arrays;
import java.util.List;

public enum Faction {

    ENGINEERS(1, "Engineers"),
    LUNATICS(2, "Lunatics"),
    NOMADS(3, "Nomads"),
    SCAVANGER(4, "Scavanger"),
    STEPPENWOLFS(5, "Steppenwolfs"),
    DAWNSCHILDREN(6, "Dawn's Children"),
    FIRESTARTERS(7, "Firestarters");

    private final int    id;
    private final String name;

    Faction(int id, String name) {
        this.id   = id;
        this.name = name;
    }

    @SuppressWarnings("unused")
    public static Faction get(int id) {
        for (Faction value : Faction.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }

    public static Faction getClosest(String str) {
        for (Faction value : Faction.values()) {
            if (value.getName().toLowerCase().startsWith(str.toLowerCase())) {
                return value;
            }
        }
        return null;
    }

    public static List<Faction> asList() {
        return Arrays.asList(Faction.values());
    }

    private int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
