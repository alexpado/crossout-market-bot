package ovh.akio.jdamodules.crossout.gameobjects;

import fr.alexpado.tools.BotUtils;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.json.JSONObject;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.crossout.PriceStatus;
import ovh.akio.jdamodules.crossout.entities.Watcher;
import ovh.akio.jdamodules.httprequest.HttpException;
import ovh.akio.jdamodules.translations.Language;

import java.awt.*;
import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Item {

    private final int     id;
    private final String  name;
    private final String  description;
    private final boolean removed;
    private final boolean craftable;

    private final int sellPrice;
    private final int buyPrice;

    private final int craftingSellSum;
    private final int craftingBuySum;

    private final long lastUpdate;

    private final Rarity rarity;

    public Item(JSONObject source) {
        this.id   = source.getInt("id");
        this.name = source.getString("name");

        if (source.get("description") == JSONObject.NULL) {
            this.description = null;
        } else {
            // HTML should be removed server-side.
            this.description = BotUtils.removeHTML(source.getString("description").replace("<br>", ""));
        }

        this.rarity          = Rarity.get(source.getInt("rarityId"));
        this.sellPrice       = source.getInt("sellPrice");
        this.buyPrice        = source.getInt("buyPrice");
        this.craftingSellSum = source.getInt("craftingSellSum");
        this.craftingBuySum  = source.getInt("craftingBuySum");
        this.removed         = source.getInt("removed") == 1;
        this.craftable       = !(craftingSellSum == 0 || craftingBuySum == 0);

        this.lastUpdate = Instant.parse(source.getString("timestamp") + ".000Z").toEpochMilli() / 1000;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private String getDescription(Language language) {
        return description == null ? language.getTranslation("item.noDescription") : description;
    }

    public boolean isRemoved() {
        return removed;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public int getBuyPrice() {
        return buyPrice;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    private String getThumbnailUrl() {
        Date   today        = Calendar.getInstance().getTime();
        String thumbnailUrl = "https://crossoutdb.com/img/items/%s.png?d=%tY%tm%td";
        return String.format(thumbnailUrl, this.getId(), today, today, today);
    }

    private String getWebUrl() {
        String webUrl = "https://crossoutdb.com/item/%s?ref=crossoutmarketbot";
        return String.format(webUrl, this.getId());
    }

    private EmbedBuilder createItemEmbed(String selfAvatar, Language language) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, selfAvatar);
        builder.setTitle(this.getName(), this.getWebUrl());
        builder.setDescription(this.getDescription(language));

        return builder;
    }

    public MessageEmbed getDiffEmbed(Language language, JDA jda, Watcher watcher) {
        String       selfAvatar   = jda.getSelfUser().getAvatarUrl();
        EmbedBuilder builder      = this.createItemEmbed(selfAvatar, language);
        String       currency     = "%,.2f %s";
        String       diffCurrency = "%,.2f";

        float  finalSellPrice         = this.sellPrice / 100.0f;
        float  finalBuyPrice          = this.buyPrice / 100.0f;
        String formattedSellPrice     = String.format(currency, finalSellPrice, language.getTranslation("item.currency"));
        String formattedBuyPrice      = String.format(currency, finalBuyPrice, language.getTranslation("item.currency"));
        float  finalSellDiffPrice     = finalSellPrice - watcher.getLastSellPrice() / 100.0f;
        float  finalBuyDiffPrice      = finalBuyPrice - watcher.getLastBuyPrice() / 100.0f;
        String formattedSellDiffPrice = String.format(diffCurrency, finalSellDiffPrice, language.getTranslation("item.currency"));
        String formattedBuyDiffPrice  = String.format(diffCurrency, finalBuyDiffPrice, language.getTranslation("item.currency"));
        String emoteSellPrice         = finalSellDiffPrice > 0 ? PriceStatus.GROW_RED.getAsMention(jda) : finalSellDiffPrice < 0 ? PriceStatus.DROP_GREEN.getAsMention(jda) : PriceStatus.UNCHANGED.getAsMention(jda);
        String emoteBuyPrice          = finalBuyDiffPrice > 0 ? PriceStatus.GROW_GREEN.getAsMention(jda) : finalBuyDiffPrice < 0 ? PriceStatus.DROP_RED.getAsMention(jda) : PriceStatus.UNCHANGED.getAsMention(jda);

        if (this.removed) {
            builder.addField("", language.getTranslation("item.removed"), false);
        } else {
            builder.addField(language.getTranslation("item.buy"), formattedSellPrice + "(" + emoteSellPrice + formattedSellDiffPrice + ")", true);
            builder.addField(language.getTranslation("item.sell"), formattedBuyPrice + "(" + emoteBuyPrice + formattedBuyDiffPrice + ")", true);
        }
        builder.setThumbnail(this.getThumbnailUrl());
        builder.setFooter(String.format(language.getTranslation("general.watchers.notice"), watcher.getId()), null);

        if (this.rarity != null) {
            builder.setColor(this.rarity.getColor());
        } else {
            builder.setColor(Color.BLACK);
        }

        return builder.build();
    }

    public MessageEmbed getEmbed(Language language, JDA jda) throws HttpException, IOException {
        String       selfAvatar = jda.getSelfUser().getAvatarUrl();
        EmbedBuilder builder    = this.createItemEmbed(selfAvatar, language);
        String       currency   = "%,.2f %s";

        float  finalSellPrice          = this.sellPrice / 100.0f;
        float  finalBuyPrice           = this.buyPrice / 100.0f;
        float  finalSellCraftPrice     = this.craftingSellSum / 100.0f;
        float  finalBuyCraftPrice      = this.craftingBuySum / 100.0f;
        String formattedSellPrice      = String.format(currency, finalSellPrice, language.getTranslation("item.currency"));
        String formattedBuyPrice       = String.format(currency, finalBuyPrice, language.getTranslation("item.currency"));
        String formattedSellCraftPrice = String.format(currency, finalSellCraftPrice, language.getTranslation("item.currency"));
        String formattedBuyCraftPrice  = String.format(currency, finalBuyCraftPrice, language.getTranslation("item.currency"));

        if (this.removed) {
            builder.addField("", language.getTranslation("item.removed"), false);
        } else {
            builder.addField(language.getTranslation("item.buy"), formattedSellPrice, true);
            builder.addField(language.getTranslation("item.sell"), formattedBuyPrice, true);

            if (this.craftable) {
                builder.addField(language.getTranslation("item.craftBuy"), formattedSellCraftPrice, true);
                builder.addField(language.getTranslation("item.craftSell"), formattedBuyCraftPrice, true);
            }
        }
        builder.setThumbnail(this.getThumbnailUrl());
        builder.setImage(String.format("http://bots.alexpado.fr:8181/chart/%s/%s/chart.png", this.id, this.lastUpdate));

        if (this.rarity != null) {
            builder.setColor(this.rarity.getColor());
        } else {
            builder.setColor(Color.BLACK);
        }

        return builder.build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Item) {
            return this.getId() == ((Item) obj).getId();
        }
        return super.equals(obj);
    }


    @Override
    public String toString() {
        return this.getName();
    }

}