package ovh.akio.jdamodules.crossout.gameobjects;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public enum Rarity {

    COMMON(1, "Common", new Color(216, 216, 216)),
    RARE(2, "Rare", new Color(0, 99, 200)),
    EPIC(3, "Epic", new Color(174, 0, 241)),
    LEGENDARY(4, "Legendary", new Color(236, 147, 58)),
    RELIC(5, "Relic", new Color(219, 87, 0));

    private final int    id;
    private final String name;
    private final Color  color;

    Rarity(int id, String name, Color color) {
        this.id    = id;
        this.name  = name;
        this.color = color;
    }

    public static Rarity get(int id) {
        for (Rarity value : Rarity.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }

    public static Rarity getClosest(String str) {
        for (Rarity value : Rarity.values()) {
            if (value.getName().toLowerCase().startsWith(str.toLowerCase())) {
                return value;
            }
        }
        return null;
    }

    public static List<Rarity> asList() {
        return Arrays.asList(Rarity.values());
    }

    private int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
