package ovh.akio.jdamodules.crossout.gameobjects;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused")
public enum Type {

    NONE(0, "None"),
    AMMUNITION(1, "Ammunition"),
    APPEARANCE(2, "Appearance"),
    AUTOCANNON(3, "Auto-cannon"),
    BOOSTER(4, "Booster"),
    MEDIUM_CABIN(5, "Medium cabin"),
    CANNON(6, "Cannon"),
    COMPONENT(7, "Component"),
    CRATE(8, "Crate"),
    DRONE(9, "Drone"),
    PAINT(10, "Paint"),
    CROSSBOW(11, "Crossbow"),
    ENGINE(12, "Engine"),
    FRONTAL_MACHINE_GUN(13, "Frontal machine gun"),
    GENERATOR(14, "Generator"),
    GRENADE_LAUNCHER(15, "Grenade launcher"),
    HOMING_MISSILE(16, "Homing missile"),
    HOMING_MISSILE_VOLLEY(17, "Homing missile volley"),
    MACHINE_GUN(18, "Machine gun"),
    MELEE_WEAPON(19, "Melee weapon"),
    MINELAYER(20, "Minelayer"),
    PROXIMITY_MINE(21, "Proximity mine"),
    PULSE_ACCELERATOR(22, "Pulse accelerator"),
    RADAR(23, "Radar"),
    RADIO(24, "Radio"),
    RESOURCE(25, "Resource"),
    SHOTGUN(26, "Shotgun"),
    SPECIAL_MODULE(27, "Special module"),
    STEALTH(28, "Stealth"),
    TRACK(29, "Track"),
    TURRET(30, "Turret"),
    TURRET_CANNON(31, "Turret cannon"),
    UNGUIDED_ROCKET(32, "Unguided rocket"),
    VISOR(33, "Visor"),
    WEAPON_COOLING(34, "Weapon cooling"),
    WHEEL(35, "Wheel"),
    MINIGUN(36, "Minigun"),
    FLAMETHROWER(37, "Flamethrower"),
    DECOR_HORN(38, "Decor. Horn"),
    DECOR_SIGNS(39, "Decor. Signs"),
    DECOR_LIGHTS(40, "Decor. Lights"),
    MECHANICAL_LEG(41, "Mechanical Leg"),
    CONSUMABLE(42, "Consumable"),
    HOWITZER(43, "Howitzer"),
    CUSTOMIZATION_KIT(44, "Customization kit"),
    META_ITEM(45, "Meta Item"),
    PLASMA_EMITTER(46, "Plasma Emitter"),
    HOVER(47, "Hover"),
    MICROFACTORY(48, "Microfactory"),
    COMBAT_LASER(49, "Combat laser"),
    TESLA_CANNON(50, "Tesla cannon"),
    PROTECTIVEFIELD(51, "Protectivefield"),
    STICKER(52, "Sticker"),
    HARPOON_CANNON(53, "Harpoon cannon"),
    AUGER(54, "Auger"),
    LIGHT_CABIN(55, "Light cabin"),
    HEAVY_CABIN(56, "Heavy cabin"),
    TANK_CABIN(57, "Tank cabin"),
    WEAPON_BOOSTER(58, "Weapon booster"),
    CATAPULT(59, "Catapult"),
    FLARE(60, "Flare"),
    REVOLVER(61, "Revolver"),
    COUPLING_MODULE(62, "Coupling module"),
    RAPIDFIRE_MACHINEGUN(63, "Rapid-fire machinegun"),
    RADIATOR(64, "Radiator"),
    COOLER(65, "Cooler"),
    PLASMA_GUN(66, "Plasma Gun");

    private final int    id;
    private final String name;

    Type(int id, String name) {
        this.id   = id;
        this.name = name;
    }

    public static Type get(int id) {
        for (Type value : Type.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }

    public static Type getClosest(String str) {
        for (Type value : Type.values()) {
            if (value.getName().toLowerCase().startsWith(str.toLowerCase())) {
                return value;
            }
        }
        return null;
    }

    public static List<Type> asList() {
        return Arrays.asList(Type.values());
    }

    /**
     * Get the id of the current Type.
     */
    private int getId() {
        return id;
    }

    /**
     * Get the name of the current Type
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
