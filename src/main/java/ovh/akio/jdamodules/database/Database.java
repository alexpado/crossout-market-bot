package ovh.akio.jdamodules.database;

import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.logger.Logger;

import java.sql.*;
import java.util.Arrays;

public class Database {

    private       Connection        connection;
    private final String            host;
    private final String            user;
    private final String            pass;
    private final String            name;
    private final int               port;
    private       PreparedStatement statement;

    /**
     * Try to establish a connection to a MySQL database with provided informations.
     *
     * @param host
     *         Remote host where the database is hosted.
     * @param user
     *         Username to use when connecting to the database.
     * @param pass
     *         Password to use when connecting to the database.
     * @param name
     *         Name of the database to connect to.
     * @param port
     *         Port to use when connecting to the database.
     */
    Database(String host, String user, String pass, String name, int port) throws SQLException {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.name = name;
        this.port = port;
        this.createConnection();
    }

    private boolean checkType(Object o, Class... clazz) {
        for (Class aClass : clazz) {
            if (o.getClass().getSimpleName().equals(aClass.getSimpleName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Try to connect to the database.
     */
    private void createConnection() throws SQLException {
        String connectionOptions = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
        this.connection = DriverManager.getConnection("jdbc:mariadb://" + this.host + ":" + this.port + "/" + this.name + connectionOptions, this.user, this.pass);
    }

    /**
     * Try to reopen the connection if its not open anymore.
     */
    private void reloadConnection() throws SQLException {
        if (connection == null || connection.isClosed() || !connection.isValid(5)) {
            this.createConnection();
        }
    }

    /**
     * Close an opened connection to the database.
     */
    public void close() throws SQLException {
        if (connection != null && !connection.isClosed() && connection.isValid(5)) {
            this.connection.close();
        }
    }

    /**
     * Set an indexed parameter with type autodetection for a statement.
     */
    private boolean setParameter(PreparedStatement statement, Object parameter, int i) throws SQLException {
        if (checkType(parameter, int.class, Integer.class)) {
            statement.setInt(i, ((int) parameter));
            return true;
        } else if (checkType(parameter, long.class, Long.class)) {
            statement.setLong(i, ((long) parameter));
            return true;
        } else if (checkType(parameter, boolean.class, Boolean.class)) {
            statement.setBoolean(i, ((boolean) parameter));
            return true;
        } else if (checkType(parameter, String.class)) {
            statement.setString(i, ((String) parameter));
            return true;
        } else if (checkType(parameter, double.class, Double.class)) {
            statement.setDouble(i, ((double) parameter));
            return true;
        } else if (checkType(parameter, float.class, Float.class)) {
            statement.setFloat(i, ((float) parameter));
            return true;
        } else if (checkType(parameter, Object.class)) {
            statement.setObject(i, parameter);
            return true;
        }
        return false;
    }

    public boolean execute(String sql, Object... parameters) {
        int i = 1;
        try {
            this.reloadConnection();
            PreparedStatement statement = this.connection.prepareStatement(sql);
            for (Object parameter : parameters) {
                if (this.setParameter(statement, parameter, i)) {
                    i++;
                }
            }
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            Logger.warn(sql);
            Logger.warn(Arrays.toString(parameters));
            ReporterModule.instance.reportError(e);
        }
        return false;
    }

    @SuppressWarnings("unused")
    public ResultSet executeAndAutoIncrement(String sql, Object... parameters) {
        int i = 1;
        try {
            this.reloadConnection();
            PreparedStatement statement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            for (Object parameter : parameters) {
                if (this.setParameter(statement, parameter, i)) {
                    i++;
                }
            }
            statement.execute();
            return statement.getGeneratedKeys();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            Logger.warn(sql);
            Logger.warn(Arrays.toString(parameters));
            ReporterModule.instance.reportError(e);
        }
        return null;
    }

    public ResultSet query(String sql, Object... parameters) {
        int i = 1;
        try {
            this.reloadConnection();
            statement = this.connection.prepareStatement(sql);
            for (Object parameter : parameters) {
                if (this.setParameter(statement, parameter, i)) {
                    i++;
                }
            }
            return statement.executeQuery();
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            Logger.warn(sql);
            Logger.warn(Arrays.toString(parameters));
            ReporterModule.instance.reportError(e);
        }
        return null;
    }

    public void close(ResultSet rs) {
        try {
            rs.close();
            statement.close();
        } catch (Exception e) {
            Logger.error(e.getMessage());
            ReporterModule.instance.reportError(e);
        }
    }

}
