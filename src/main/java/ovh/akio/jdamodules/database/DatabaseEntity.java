package ovh.akio.jdamodules.database;

import java.sql.SQLException;

@SuppressWarnings("unused")
public interface DatabaseEntity {

    boolean select(Database database) throws SQLException;

    boolean insert(Database database);

    boolean delete(Database database);

    boolean update(Database database);

}
