package ovh.akio.jdamodules.database;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.config.JSONConfiguration;

import java.sql.SQLException;

public class DatabaseModule extends JDAModule {

    public DatabaseModule(JDABot bot) throws Exception {
        super(bot);
    }

    /**
     * Indicate whenever a configuration file is required for this module.
     *
     * @return True to enable configuration for this module. False instead.
     */
    @Override
    public boolean doNeedConfiguration() {
        return true;
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "database";
    }


    /**
     * Create a new connection to a database with configuration data.
     *
     * @return Database connection instance.
     */
    public Database createConnection() throws SQLException {
        JSONConfiguration configuration = this.getConfiguration();

        String dbHost = configuration.getString("host");
        String dbName = configuration.getString("name");
        String dbUser = configuration.getString("user");
        String dbPass = configuration.getString("pass");
        int    dbPort = configuration.getInt("port");

        return new Database(dbHost, dbUser, dbPass, dbName, dbPort);
    }

    @Override
    public void onConfigurationCreated(JSONConfiguration configuration) {
        configuration.put("host", "localhost");
        configuration.put("name", "databaseName");
        configuration.put("user", "myUsername");
        configuration.put("pass", "myPassword");
        configuration.put("port", 3306);
        super.onConfigurationCreated(configuration);
    }

}
