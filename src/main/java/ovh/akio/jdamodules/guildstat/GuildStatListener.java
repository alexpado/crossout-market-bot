package ovh.akio.jdamodules.guildstat;

import fr.alexpado.tools.timers.TickTimer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import static ovh.akio.jdamodules.guildstat.MessageType.*;


public class GuildStatListener extends ListenerAdapter {


    private GuildStatModule module;

    private       int                    commandExecutedCount      = 0;
    private final HashMap<Long, Integer> guildCommandExecutedCount = new HashMap<>();
    private       int                    guildCount                = 0;
    private       long                   userCount                 = 0;
    private       MessageType            type                      = MessageType.USER;
    private       JDA                    jda;
    private       boolean                crossoutDbAvailable       = true;

    public GuildStatListener(GuildStatModule module) {
        this.module = module;
    }

    private void refreshMessage() {
        this.refreshMessage(this.jda);
    }

    private void refreshMessage(JDA jda) {
        this.checkCrossoutDbAvailabilty();
        if (this.isCrossoutDbAvailable()) {
            jda.getPresence().setStatus(OnlineStatus.ONLINE);
            switch (this.type) {
                case GUILD:
                    jda.getPresence().setActivity(Activity.of(Activity.ActivityType.DEFAULT, this.guildCount + " servers."));
                    break;
                case COMMAND:
                    jda.getPresence().setActivity(Activity.of(Activity.ActivityType.DEFAULT,this.commandExecutedCount + " commands executed."));
                    break;
                case USER:
                    jda.getPresence().setActivity(Activity.of(Activity.ActivityType.DEFAULT,this.userCount + " users."));
                    break;
            }
        } else {
            jda.getPresence().setStatus(OnlineStatus.DO_NOT_DISTURB);
            jda.getPresence().setActivity(Activity.of(Activity.ActivityType.DEFAULT,"to repair CrossoutDB :c"));
        }
    }


    private void checkCrossoutDbAvailabilty() {
        try {
            URL               url        = new URL("https://crossoutdb.com/tools");
            HttpURLConnection connection = ((HttpURLConnection) url.openConnection());
            connection.setRequestMethod("GET");
            // Set custom headers here to let CrossoutDB knows where the request come from.
            connection.setRequestProperty("X-DiscordBot", "CrossoutMarketBot");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            reader.close();
            this.crossoutDbAvailable = true;
        } catch (IOException e) {
            this.crossoutDbAvailable = false;
        }
    }

    private boolean isCrossoutDbAvailable() {
        return this.crossoutDbAvailable;
    }

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        this.onEnable(event.getJDA());
    }

    public void onEnable(JDA jda) {
        this.guildCount = jda.getGuilds().size();
        this.userCount  = jda.getUsers().stream().filter(user -> !user.isBot()).count();
        this.jda        = jda;
        new TickTimer(onTick -> {
            switch (this.type) {
                case GUILD:
                    this.type = MessageType.COMMAND;
                    break;
                case COMMAND:
                    this.type = MessageType.USER;
                    break;
                case USER:
                    this.type = MessageType.GUILD;
                    break;
            }
            this.refreshMessage();
        }, 10000);
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        this.jda        = event.getJDA();
        this.guildCount = event.getJDA().getGuilds().size();
        this.userCount  = event.getJDA().getUsers().size();
        this.refreshMessage(event.getJDA());
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        this.jda        = event.getJDA();
        this.guildCount = event.getJDA().getGuilds().size();
        this.userCount  = event.getJDA().getUsers().size();
        this.refreshMessage(event.getJDA());
    }

    public int getGuildCommandExecutedCount(Guild guild) {
        return this.guildCommandExecutedCount.getOrDefault(guild.getIdLong(), 0);
    }

    public int getCommandExecutedCount() {
        return commandExecutedCount;
    }

    public void incrementCommand(Guild guild) {
        this.guildCommandExecutedCount.compute(guild.getIdLong(), (k, v) -> v == null ? 1 : v + 1);
        this.commandExecutedCount++;
        this.refreshMessage(guild.getJDA());
    }

}
