package ovh.akio.jdamodules.guildstat;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.commands.JDACommandExecutor;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import ovh.akio.jdamodules.guildstat.commands.StatsCommand;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;


/**
 * Guilds Statistics Modules. Allows counting commands per guild, guild count, guild leaderboard etc...
 */
public class GuildStatModule extends JDAModule {

    private GuildStatListener listener;

    public GuildStatModule(JDABot bot) throws Exception {
        super(bot);
        this.listener = new GuildStatListener(this);
    }

    @Override
    public List<JDACommandExecutor> getCommandExecutors() {
        List<JDACommandExecutor> commands = super.getCommandExecutors();
        commands.add(new StatsCommand(this));
        return commands;
    }

    /**
     * Retrieve a list of listener that will be registered when the module get itself registered.
     *
     * @return A list of listener.
     */
    @Nonnull
    @Override
    public List<ListenerAdapter> getListeners() {
        return Collections.singletonList(this.listener);
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "guildstat";
    }

    @Override
    public void onCommandExecuted(CommandEvent event) {
        this.listener.incrementCommand(event.getGuild());
    }

    public GuildStatListener getListener() {
        return listener;
    }

}
