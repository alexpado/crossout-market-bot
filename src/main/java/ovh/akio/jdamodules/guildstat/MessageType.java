package ovh.akio.jdamodules.guildstat;

public enum MessageType {
    GUILD,
    COMMAND,
    USER
}
