package ovh.akio.jdamodules.guildstat.commands;

import fr.alexpado.RunBot;
import fr.alexpado.tools.timers.TimeConverter;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;
import ovh.akio.jdamodules.crossout.CrossoutModule;
import ovh.akio.jdamodules.guildstat.GuildStatModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StatsCommand extends TranslatableCommand {

    public StatsCommand(JDAModule module) {
        super(module, "stats");
    }

    /**
     * Gets all aliases that are able to trigger this command.
     * If an alias declared here is used as a command label, the label will have the highest priority and this command
     * won't be executed.
     *
     * @return A list of alias to trigger this command.
     */
    @NotNull
    @Override
    public List<String> getAliases() {
        List<String> aliases = super.getAliases();
        aliases.add("stat");
        return aliases;
    }

    public GuildStatModule getCastedModule() {
        return ((GuildStatModule) this.getModule());
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            Optional<CrossoutModule> optionalCrossoutModule = this.getModule().getBot().getModule(CrossoutModule.class);

            if (!optionalCrossoutModule.isPresent()) {
                this.sendError(message, "Unable to retrieve the crossout module.");
                return;
            }

            CrossoutModule crossoutModule = optionalCrossoutModule.get();


            try {
                String       uptime               = new TimeConverter((System.currentTimeMillis() - RunBot.startTime) / 1000).toString();
                int          watcherCount         = crossoutModule.getWatcherManager().getWatcherCount();
                int          guildCount           = event.getJDA().getGuilds().size();
                int          commandExecuted      = this.getCastedModule().getListener().getGuildCommandExecutedCount(event.getGuild());
                int          totalCommandExecuted = this.getCastedModule().getListener().getCommandExecutedCount();
                List<Guild>  cLeaderboard         = new ArrayList<>(event.getJDA().getGuilds());
                List<Guild>  mLeaderboard         = new ArrayList<>(event.getJDA().getGuilds());
                EmbedBuilder builder              = new EmbedBuilder();
                User         user                 = event.getSelfUser();

                StringBuilder activeBuilder  = new StringBuilder();
                StringBuilder biggestBuilder = new StringBuilder();

                mLeaderboard.sort((a, b) -> Integer.compare(b.getMembers().size(), a.getMembers().size()));
                cLeaderboard.sort((a, b) -> Integer.compare(this.getCastedModule().getListener().getGuildCommandExecutedCount(b), this.getCastedModule().getListener().getGuildCommandExecutedCount(a)));

                cLeaderboard = cLeaderboard.stream().filter(guild -> this.getCastedModule().getListener().getGuildCommandExecutedCount(guild) > 0).collect(Collectors.toList());

                mLeaderboard = mLeaderboard.subList(0, Math.min(mLeaderboard.size(), 5));
                cLeaderboard = cLeaderboard.subList(0, Math.min(cLeaderboard.size(), 5));


                builder.setAuthor(language.getTranslation("general.invite"), CrossoutModule.inviteUrl, user.getAvatarUrl());


                builder.addField(language.getTranslation("command.stats.uptime"), uptime, false);
                builder.addField(language.getTranslation("command.stats.watchers"), Integer.toString(watcherCount), false);
                builder.addField(language.getTranslation("command.stats.discord"), Integer.toString(guildCount), false);
                builder.addField(language.getTranslation("command.stats.executions"), Integer.toString(commandExecuted), false);
                builder.addField(language.getTranslation("command.stats.totalExecutions"), Integer.toString(totalCommandExecuted), false);

                for (Guild guild : mLeaderboard) {
                    biggestBuilder.append(guild.getMembers().size()).append(" > ").append(guild.getName());
                }

                for (Guild guild : cLeaderboard) {
                    activeBuilder.append(this.getCastedModule().getListener().getGuildCommandExecutedCount(guild)).append(" > ").append(guild.getName());
                }

                builder.addField(language.getTranslation("command.stats.guild.active"), activeBuilder.toString(), false);
                builder.addField(language.getTranslation("command.stats.guild.biggest"), biggestBuilder.toString(), false);


                message.editMessage(builder.build()).queue();


            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
