package ovh.akio.jdamodules.httprequest;

import org.json.JSONArray;
import ovh.akio.jdamodules.crossout.gameobjects.Item;
import ovh.akio.logger.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CrossoutWebRequest {

    private static final String API_ROOT = "https://crossoutdb.com/api/v1";

    /**
     * Create a connection to the CrossoutDB's API with custom header.
     *
     * @param target
     *         URL to request (API ROOT + ENDPOINT)
     *
     * @return Connection instance
     */
    private HttpURLConnection createConnection(String target) throws HttpException {
        try {
            URL               url        = new URL(target);
            HttpURLConnection connection = ((HttpURLConnection) url.openConnection());
            connection.setRequestMethod("GET");
            // Set custom headers here to let CrossoutDB knows where the request come from.
            connection.setRequestProperty("X-DiscordBot", "CrossoutMarketBot");
            Logger.debug("[HTTP] " + target);
            return connection;
        } catch (IOException e) {
            throw new HttpException(e);
        }
    }

    /**
     * Retrieve an array of result from the given URL.
     *
     * @param target
     *         URL to request (API ROOT + ENDPOINT)
     *
     * @return Results in a JSONArray instance
     */
    private JSONArray getJsonArray(String target) throws HttpException {
        try {
            StringBuilder     result     = new StringBuilder();
            HttpURLConnection connection = this.createConnection(target);

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String         line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();

            return new JSONArray(result.toString());
        } catch (IOException e) {
            throw new HttpException(e);
        }
    }

    /**
     * Get specific item from the CrossoutDB's API
     *
     * @return JSONObject containing item info (or null if no item is found)
     */
    public Item getItem(int itemId) throws HttpException {
        String    ITEM  = "/item";
        JSONArray array = this.getJsonArray(API_ROOT + ITEM + "/" + itemId);
        if (array.length() == 0) {
            return null;
        }
        return new Item(array.getJSONObject(0));
    }

    /**
     * Return all items found with the provided search settings from the CrossoutDB'API
     *
     * @param searchSettings
     *         All settings to use for the search
     */
    public ArrayList<Item> find(HashMap<String, String> searchSettings) throws HttpException {
        List<String> searchParameter = new ArrayList<>();

        searchSettings.forEach((key, value) -> {
            switch (key) {
                case "rarity":
                    searchParameter.add("rarity=" + value);
                    break;
                case "category":
                    searchParameter.add("category=" + value);
                    break;
                case "faction":
                    searchParameter.add("faction=" + value);
                    break;
                case "query":
                    searchParameter.add("query=" + value);
                    break;
                case "removedItems":
                    searchParameter.add("removedItems=" + value);
                    break;
                case "metaItems":
                    searchParameter.add("metaItems=" + value);
                    break;
            }
        });

        String          ITEMS = "/items";
        JSONArray       array = this.getJsonArray(API_ROOT + ITEMS + "?" + String.join("&", searchParameter));
        ArrayList<Item> items = new ArrayList<>();
        for (int i = 0 ; i < array.length() ; i++) {
            items.add(new Item(array.getJSONObject(i)));
        }
        return items;
    }

    public List<List<Integer>> getPriceEvolution(int id, long end) throws HttpException {
        String    MARKET    = "/market-all/%s?startTimestamp=%s&endTimestamp=%s";
        JSONArray priceList = this.getJsonArray(API_ROOT + String.format(MARKET, id, (end - (3600 * 5)), end));

        List<Integer> sellPrices = new ArrayList<>();
        List<Integer> buyPrices  = new ArrayList<>();


        for (int i = 0 ; i < priceList.length() ; i++) {
            JSONArray priceInfo = priceList.getJSONArray(i);
            int       buyPrice  = priceInfo.getInt(1);
            int       sellPrice = priceInfo.getInt(2);

            sellPrices.add(sellPrice);
            buyPrices.add(buyPrice);

        }
        return Arrays.asList(sellPrices, buyPrices);
    }

}
