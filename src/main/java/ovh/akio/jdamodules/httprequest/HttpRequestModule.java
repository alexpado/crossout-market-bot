package ovh.akio.jdamodules.httprequest;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;

public class HttpRequestModule extends JDAModule {

    public HttpRequestModule(JDABot bot) {
        super(bot);
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "httpreq";
    }

    /**
     * Prepare a request to the CrossoutDB's API.
     *
     * @return Instance of the CrossoutDB WebRequest.
     */
    public CrossoutWebRequest createCrossoutRequest() {
        return new CrossoutWebRequest();
    }

}
