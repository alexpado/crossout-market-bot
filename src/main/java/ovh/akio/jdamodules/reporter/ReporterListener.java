package ovh.akio.jdamodules.reporter;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

public class ReporterListener extends ListenerAdapter {

    private ReporterModule module;
    private JDA            jda;

    public ReporterListener(ReporterModule module) {
        this.module = module;
    }

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        this.jda = event.getJDA();
    }

    public JDA getJda() {
        return jda;
    }

}
