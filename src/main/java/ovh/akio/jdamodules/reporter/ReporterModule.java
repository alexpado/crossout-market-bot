package ovh.akio.jdamodules.reporter;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import ovh.akio.logger.Logger;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReporterModule extends JDAModule {

    public static ReporterModule instance = null;

    private ReporterListener listener;

    public ReporterModule(JDABot bot) {
        super(bot);
        this.listener = new ReporterListener(this);
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "reporter";
    }

    public void reportError(Throwable e) {
        this.reportError(null, e);
    }

    public void reportError(CommandEvent event, Throwable e) {
        JDA   jda       = event == null ? this.listener.getJda() : event.getJDA();
        long  guildId   = 508012982287073280L;
        Guild guild     = jda.getGuildById(guildId);
        long  channelId = 508020752994271242L;
        assert guild != null;
        TextChannel channel = guild.getTextChannelById(channelId);

        MessageBuilder builder = new MessageBuilder();
        builder.append(e.getMessage());

        List<String> exceptionLogs  = this.getCustomStackTrace(e);
        String       exceptionTrace = String.join("\n", exceptionLogs);

        Logger.error(e.getMessage());
        assert channel != null;
        channel.sendFile(exceptionTrace.getBytes(), "error.log").queue();
    }

    private List<String> getCustomStackTrace(Throwable aThrowable) {
        final StringBuilder result = new StringBuilder();
        result.append(aThrowable.toString());
        final String NEW_LINE = System.getProperty("line.separator");
        result.append(NEW_LINE);

        for (StackTraceElement element : aThrowable.getStackTrace()) {
            result.append(element);
            result.append(NEW_LINE);
        }

        List<String> stacktrace = new ArrayList<>();

        for (int i = 0 ; i < result.toString().length() ; i += 1994) {
            if (i + 1994 > result.toString().length()) {
                stacktrace.add(result.toString().substring(i));
            } else {
                stacktrace.add(result.toString().substring(i, i + 1994));
            }
        }
        return stacktrace;
    }


    @Nonnull
    @Override
    public List<ListenerAdapter> getListeners() {
        return Collections.singletonList(this.listener);
    }

    public ReporterListener getListener() {
        return listener;
    }

}
