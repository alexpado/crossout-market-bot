package ovh.akio.jdamodules.translations;

public enum BotLocale {

    FRENCH("French", "Français", "fr"),
    ENGLISH("English", "English", "en");

    private final String englishName;
    private final String localeName;
    private final String tag;

    BotLocale(String englishName, String localeName, String tag) {
        this.englishName = englishName;
        this.localeName  = localeName;
        this.tag         = tag;
    }

    public String getTag() {
        return tag;
    }

    private boolean equals(String identifier) {
        if (this.englishName.toLowerCase().equals(identifier.toLowerCase())) {
            return true;
        }
        if (this.localeName.toLowerCase().equals(identifier.toLowerCase())) {
            return true;
        }
        return this.tag.toLowerCase().equals(identifier.toLowerCase());
    }

    public static BotLocale get(String identifier) {
        return BotLocale.get(identifier, ENGLISH);
    }

    public static BotLocale get(String identifier, BotLocale defaultLocale) {
        for (BotLocale value : BotLocale.values()) {
            if (value.equals(identifier)) {
                return value;
            }
        }
        return defaultLocale;
    }
}
