package ovh.akio.jdamodules.translations;

import me.alexpado.jdamodules.config.JSONConfiguration;
import ovh.akio.logger.Logger;

import javax.security.auth.login.Configuration;
import java.io.File;

public class Language {

    private final BotLocale         locale;
    private       JSONConfiguration translations;

    Language(BotLocale locale) throws TranslationException {
        this.locale = locale;
        File file = new File("configs/locale/" + locale.getTag() + ".json");

        if (!file.exists()) {
            throw new TranslationException(new Exception("Language not supported."));
        }

        try {
            this.translations = new JSONConfiguration(file, null);
        } catch (Exception e) {
            throw new TranslationException(e);
        }
    }

    /**
     * Get the translation for the provided key.
     *
     * @param key
     *         Key identifier for the translation
     */
    public String getTranslation(String key) {
        if (!this.translations.has(key)) {
            Logger.warn(String.format("`%s` not found in `%s`. Looking up in default language.", key, this.locale.getTag()));
            return null;
        }
        return this.translations.getString(key);
    }

    /**
     * Get the locale for the current translation language
     */
    public BotLocale getLocale() {
        return locale;
    }

}
