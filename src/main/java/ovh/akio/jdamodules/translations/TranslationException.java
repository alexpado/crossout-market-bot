package ovh.akio.jdamodules.translations;

@SuppressWarnings("ALL")
public class TranslationException extends Throwable {

    private final Exception exception;

    /**
     * Constructs a new throwable with {@code null} as its detail message. The cause is not initialized, and may
     * subsequently be initialized by a call to {@link #initCause}.
     * <p>The {@link #fillInStackTrace()} method is called to initialize
     * the stack trace data in the newly created throwable.
     */
    public TranslationException(Exception exception) {
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

}
