package ovh.akio.jdamodules.translations;


import me.alexpado.jdamodules.JDABot;
import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.commands.JDACommandExecutor;
import me.alexpado.jdamodules.config.JSONConfiguration;
import me.alexpado.jdamodules.events.CommandEvent;
import net.dv8tion.jda.api.entities.User;
import ovh.akio.jdamodules.crossout.entities.DiscordChannel;
import ovh.akio.jdamodules.crossout.entities.DiscordGuild;
import ovh.akio.jdamodules.crossout.entities.DiscordUser;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.commands.ResetLanguageCommand;
import ovh.akio.jdamodules.translations.commands.SetLanguageCommand;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TranslationModule extends JDAModule {

    private static final String CONFIG_DEFAULT_LANG = "default.language";

    private final ArrayList<Language> languages = new ArrayList<>();
    private       Language            defaultLanguage;

    public TranslationModule(JDABot bot) throws TranslationException, Exception {
        super(bot);

        String    defaultLanguage = this.getConfiguration().getString(TranslationModule.CONFIG_DEFAULT_LANG);
        BotLocale defaultLocale   = BotLocale.get(defaultLanguage);
        this.defaultLanguage = new Language(defaultLocale);
        this.languages.add(this.defaultLanguage);
    }

    /**
     * Get this module name that will be also used for the configuration file name.
     * Should be unique or the configuration files will be messed up.
     * Avoid special character as each OS have its own rules for file.
     *
     * @return The module name that will be used in logs when needed.
     */
    @Override
    public String getName() {
        return "translation";
    }

    /**
     * Indicate whenever a configuration file is required for this module.
     *
     * @return True to enable configuration for this module. False instead.
     */
    @Override
    public boolean doNeedConfiguration() {
        return true;
    }

    /**
     * Called when the configuration file is created. Use it to set default values in your configuration and to help
     * users define their own settings.
     */
    @Override
    public void onConfigurationCreated(JSONConfiguration configuration) {
        configuration.put(TranslationModule.CONFIG_DEFAULT_LANG, BotLocale.ENGLISH.getTag());
        super.onConfigurationCreated(configuration);
    }

    /**
     * Get the default language provided in the configuration file.
     */
    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    /**
     * Get the language defined by the provided locale or load it.
     */
    private Language findLanguage(BotLocale locale) throws TranslationException {
        try {
            return this.languages.stream().filter(language -> language.getLocale().equals(locale)).findFirst().orElse(new Language(locale));
        } catch (Exception e) {
            throw new TranslationException(e);
        }
    }

    /**
     * Get the language defined by the provided event or load it.
     */
    public Language findLanguage(CommandEvent event) throws TranslationException {
        BotLocale locale = this.getLocaleFor(event);
        return this.findLanguage(locale);
    }

    /**
     * Get the language defined by the provided event or load it.
     */
    public Language findLanguage(User user) throws TranslationException {

        Optional<DatabaseModule> optionalDatabaseModule = this.getBot().getModule(DatabaseModule.class);
        Optional<ReporterModule> optionalReporterModule = this.getBot().getModule(ReporterModule.class);

        if (!optionalDatabaseModule.isPresent()) {
            return this.defaultLanguage;
        }

        DatabaseModule dbModule = optionalDatabaseModule.get();

        Language language;
        Database database = null;
        try {
            database = dbModule.createConnection();
            DiscordUser dUser = new DiscordUser(user);

            if (!dUser.select(database)) {
                dUser.insert(database);
            }
            language = this.findLanguage(dUser.getLocale());
        } catch (SQLException e) {
            optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            language = this.findLanguage(BotLocale.ENGLISH);
        } finally {
            if (database != null) {
                try {
                    database.close();
                } catch (SQLException e) {
                    optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                }
            }
        }
        return language;
    }

    /**
     * Get the translation of the provided translation key.
     */
    @SuppressWarnings("unused")
    public String getTranslation(CommandEvent event, String key) throws TranslationException {
        try {
            BotLocale locale      = this.getLocaleFor(event);
            Language  language    = this.findLanguage(locale);
            String    translation = language.getTranslation(key);

            if (translation == null) {
                language = this.getDefaultLanguage();
                return language.getTranslation(key);
            }
            return translation;
        } catch (Exception e) {
            throw new TranslationException(e);
        }
    }

    /**
     * Retrieve the locale defined in the provided event context.
     */
    private BotLocale getLocaleFor(CommandEvent event) {
        Optional<DatabaseModule> optionalDatabaseModule = this.getBot().getModule(DatabaseModule.class);
        Optional<ReporterModule> optionalReporterModule = this.getBot().getModule(ReporterModule.class);

        if (!optionalDatabaseModule.isPresent()) {
            return this.defaultLanguage.getLocale();
        }

        DatabaseModule dbModule = optionalDatabaseModule.get();

        Database  database = null;
        BotLocale locale;

        try {
            database = dbModule.createConnection();
            DiscordChannel channel = new DiscordChannel(event.getChannel());
            DiscordGuild   guild   = new DiscordGuild(event.getGuild());

            if (!channel.select(database)) {
                if (!guild.select(database)) {
                    guild.insert(database);
                }
                locale = guild.getLocale();
            } else {
                locale = channel.getLocale();
            }
        } catch (SQLException e) {
            optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            locale = BotLocale.ENGLISH;
        } finally {
            if (database != null) {
                try {
                    database.close();
                } catch (SQLException e) {
                    optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    e.printStackTrace();
                }
            }
        }
        return locale;
    }

    @Override
    public List<JDACommandExecutor> getCommandExecutors() {
        List<JDACommandExecutor> commands = super.getCommandExecutors();
        commands.add(new SetLanguageCommand(this));
        commands.add(new ResetLanguageCommand(this));
        return commands;
    }

}
