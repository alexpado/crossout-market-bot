package ovh.akio.jdamodules.translations.commands;

import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.crossout.entities.DiscordChannel;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.sql.SQLException;
import java.util.Optional;

public class ResetLanguageCommand extends TranslatableCommand {

    public ResetLanguageCommand(JDAModule module) {
        super(module, "rlang");
    }


    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {
            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, language.getTranslation("general.database.error"));
                return;
            }

            DatabaseModule dbModule = optionalDatabaseModule.get();
            Database       database = null;

            try {
                database = dbModule.createConnection();
                DiscordChannel channel = new DiscordChannel(event.getChannel());

                if (!channel.select(database)) {
                    this.sendError(message, language.getTranslation("command.rlang.undefined"));
                } else {
                    if (channel.delete(database)) {
                        this.sendInfo(message, language.getTranslation("command.rlang.removed"));
                    } else {
                        this.sendError(message, language.getTranslation("general.database.error"));
                    }
                }
            } catch (SQLException e) {
                this.sendError(message, language.getTranslation("general.database.error"), e.getMessage());
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        this.sendError(message, this.getTranslation(language, "general.database.wtf"));
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                    }
                }
            }
        });
    }

}
