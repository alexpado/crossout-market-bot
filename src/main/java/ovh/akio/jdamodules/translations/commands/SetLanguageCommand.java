package ovh.akio.jdamodules.translations.commands;


import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.crossout.entities.DiscordChannel;
import ovh.akio.jdamodules.crossout.entities.DiscordGuild;
import ovh.akio.jdamodules.database.Database;
import ovh.akio.jdamodules.database.DatabaseModule;
import ovh.akio.jdamodules.reporter.ReporterModule;
import ovh.akio.jdamodules.translations.BotLocale;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.translatables.TranslatableCommand;

import java.sql.SQLException;
import java.util.Optional;

public class SetLanguageCommand extends TranslatableCommand {

    public SetLanguageCommand(JDAModule module) {
        super(module, "lang");
    }

    /**
     * Called when this command is executed in the chat.
     */
    @Override
    public void execute(CommandEvent event, Language language) {
        this.sendWaiting(event, message -> {

            Optional<DatabaseModule> optionalDatabaseModule = this.getModule().getBot().getModule(DatabaseModule.class);
            Optional<ReporterModule> optionalReporterModule = this.getModule().getBot().getModule(ReporterModule.class);

            if (!optionalDatabaseModule.isPresent()) {
                this.sendError(message, language.getTranslation("general.database.error"));
                return;
            }

            DatabaseModule dbModule       = optionalDatabaseModule.get();
            Database       database       = null;
            String         languageString = String.join(" ", event.getArgs().subList(1, event.getArgs().size()));
            boolean        onChannel      = false;

            if (event.getArgs().indexOf("--channel") != -1) {
                languageString = languageString.replace("--channel", "").trim();
                onChannel      = true;
            } else if (event.getArgs().indexOf("-c") != -1) {
                languageString = languageString.replace("-c", "").trim();
                onChannel      = true;
            }

            BotLocale locale = BotLocale.get(languageString, null);

            if (locale == null) {
                this.sendError(message, language.getTranslation("command.lang.notFound"));
                return;
            }

            try {
                database = dbModule.createConnection();
                if (onChannel) {
                    DiscordChannel channel = new DiscordChannel(event.getChannel());
                    if (!channel.select(database)) {
                        if (!channel.insert(database)) {
                            throw new SQLException();
                        }
                    }
                    channel.setLocale(locale);
                    if (channel.update(database)) {
                        this.sendInfo(message, language.getTranslation("command.lang.switch.channel"));
                    } else {
                        this.sendError(message, language.getTranslation("general.database.error"));
                    }
                } else {
                    DiscordGuild guild = new DiscordGuild(event.getGuild());
                    if (!guild.select(database)) {
                        guild.insert(database);
                    }
                    guild.setLocale(locale);
                    guild.update(database);
                    this.sendInfo(message, language.getTranslation("command.lang.switch.guild"));
                }
            } catch (SQLException e) {
                optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                this.sendError(message, language.getTranslation("general.database.error"));
            } finally {
                if (database != null) {
                    try {
                        database.close();
                    } catch (SQLException e) {
                        optionalReporterModule.ifPresent(reporter -> reporter.reportError(e));
                        this.sendError(message, language.getTranslation("general.database.wtf"));
                    }
                }
            }
        });
    }

}
