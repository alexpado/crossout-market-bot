package ovh.akio.jdamodules.translations.translatables;


import me.alexpado.jdamodules.JDAModule;
import me.alexpado.jdamodules.commands.JDACommandExecutor;
import me.alexpado.jdamodules.events.CommandEvent;
import ovh.akio.jdamodules.translations.Language;
import ovh.akio.jdamodules.translations.TranslationException;
import ovh.akio.jdamodules.translations.TranslationModule;
import ovh.akio.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class allows commands from modules to be fully compatible with translation. You can translate your commands
 * without this class, but this one allows more clean code and manage the creation of translation path inside your
 * translations files.
 */
public abstract class TranslatableCommand extends JDACommandExecutor {

    /**
     * Create a new translatable command.
     *
     * @param module
     *         Module to link to this command.
     */
    protected TranslatableCommand(JDAModule module, String label) {
        super(module, label);
    }

    /**
     * Get this command description that can be retrieved in another module/command (Help command for example) NOTE :
     * This can always return null, as the help command is overridden by the translation module. This method can't be
     * overridden because it would be never used.
     */
    @Override
    public final String getDescription() {
        return null;
    }

    /**
     * Retrieve the translated description of this command.
     *
     * @param language
     *         The language to use when retrieving the translation
     *
     * @return The translated description of this command
     */
    public final String getDescription(Language language) {
        return this.getTranslation(language, "description");
    }

    /**
     * Retrieve a specific translation specified with the key. The path is auto-generated, you just have to give your
     * key (e.g : `error` instead of `commands.yourcommand.error`). If a dot is present in the key, it will retrieve the
     * translation without prepending the path.
     *
     * @param language
     *         The language to use when retrieving the translation
     * @param key
     *         The key to retrieve. Path will be prepended.
     *
     * @return The translation of the given key with the given language.
     */
    public final String getTranslation(Language language, String key) {
        if (key.contains(".")) {
            // If key contains a dot, this is an absolute translation path
            return language.getTranslation(key);
        }
        // Else, just prepend the command translation path
        List<String> translationPath = new ArrayList<>();
        translationPath.add("command");
        translationPath.add(this.getLabel());
        translationPath.add(key);
        return language.getTranslation(String.join(".", translationPath));
    }

    /**
     * Called when this command is executed in the chat.
     *
     * @param event
     *         Event that triggered this command
     */
    @Override
    public final void runCommand(CommandEvent event) {
        Optional<TranslationModule> optionalTranslationModule = this.getModule().getBot().getModule(TranslationModule.class);

        if (!optionalTranslationModule.isPresent()) {
            event.getChannel().sendMessage("Unable to load translation module.").queue();
        }

        TranslationModule translationModule = optionalTranslationModule.get();
        Language language;
        try {
            language = translationModule.findLanguage(event);
        } catch (TranslationException e) {
            Logger.warn(e.getMessage());
            language = translationModule.getDefaultLanguage();
        }
        this.execute(event, language);
    }

    /**
     * Called when this command is executed in the chat.
     *
     * @param event
     *         Event that triggered this command
     * @param language
     *         Language associated with this event
     */
    protected abstract void execute(CommandEvent event, Language language);

}
